from django.db import models
from summa.summarizer import summarize

# Create your models here.
class Prueba(models.Model):
    texto = models.TextField(blank=True)
    nroPalabras = models.IntegerField(null=True, blank=True,  default = 50)
    lenguaje = models.TextField(blank=True)
    resumen = models.TextField(blank=True)
    def resumen(self):
        
       return summarize(self.texto, words= self.nroPalabras, language= self.lenguaje)