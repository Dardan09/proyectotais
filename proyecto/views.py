from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from .serializers import PruebaSerializer
from .models import Prueba

from django.shortcuts import render

class PruebaViewSet(ModelViewSet):
    queryset = Prueba.objects.all()
    serializer_class = PruebaSerializer
